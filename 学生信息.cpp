#include "pch.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <list>
using namespace std;

class studentr
{
public:
	int no;
	string name;
	string g;

	void display()
	{
		cout << "----" << this->name << " " << this->no << "----\n";
		cout << "性别：" << this->g << endl;
		cout << "--------" << endl;
	}
};

list<studentr> studentrList;
void addstudentr()
{
	studentrList.push_back(studentr());
	studentr &t = studentrList.back();
	cout << "请输入要录入的学生信息：" << endl;
	cout << "学号：";	cin >> t.no;
	cout << "姓名：";	cin >> t.name;
	cout << "性别（男/女）：";		cin >> t.g;
	cout << "录入完成" << endl;
}

studentr* findstudentr(string name)
{
	for (studentr& t : studentrList)
	{
		if (t.name == name)
			return &t;
	}
	return nullptr;
}

studentr* findstudentr(int no)
{
	for (studentr& t : studentrList)
	{
		if (t.no == no)
			return &t;
	}
	return nullptr;
}

studentr* inputName()
{
	string name;
	cout << "请输入学生姓名：";		cin >> name;
	studentr* t = findstudentr(name);
	return t;
}

int main()
{
	while (1)
	{
		int f;
		printf("1.录入新的学生信息\n");
		printf("2.查询学生信息\n");
		printf("3.显示全部学生信息\n");
		printf("请输入你要选择的操作前的数字：");
		scanf_s("%d", &f);
		if (f >= 1 && f <= 3)
		{

			switch (f)
			{
			case 1:
			{
				addstudentr();
				break;
			}
			case 2:
			{
				studentr* t = inputName();
				if (t == nullptr)
					printf("输入的学生不存在\n");
				else
					(*t).display();
				break;
			}
			case 3:
			{
				for (studentr &t : studentrList)
					t.display();
			}
			}
		}
		else
		{
			printf("输入错误！\n");
		}
	}
}