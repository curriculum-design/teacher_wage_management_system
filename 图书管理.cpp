#include "pch.h"
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class book
{
public:
	string name;
	string id;
	string pub;
	string type;
	int num;
	int restNum;

	void display()
	{
		cout << "----" << this->name << " " << this->id << "----\n";
		cout << "出版社：" << this->pub << endl;
		cout << "图书类型：" << this->type << endl;
		cout << "总库存：" << this->num << endl;
		cout << "剩余库存：" << this->restNum << endl;
		cout << "--------" << endl;
	}
};

vector<book> bookList;
void addbook()
{
	bookList.push_back(book());
	book &t = bookList.back();
	cout << "请输入要录入的图书信息：" << endl;
	cout << "图书名称：";	cin >> t.name;
	cout << "图书ID：";	cin >> t.id;
	cout << "出版社：";	cin >> t.pub;
	cout << "图书类型：";	cin >> t.type;
	cout << "总库存：";	cin >> t.num;
	cout << "剩余库存：";	cin >> t.restNum;
	cout << "录入完成" << endl;
}

book* findbook(string name)
{
	for (int i = 0; i < bookList.size(); i++)
	{
		if (bookList[i].name == name)
			return &bookList[i];
	}
	return NULL;
}

bool delbook(string name)
{
	for (vector<book>::iterator it = bookList.begin(); it != bookList.end(); ++it)
	{
		if ((*it).name == name)
		{
			it = bookList.erase(it);
			return true;
		}
		/*else
			++it;*/
	}
	return false;
}

int main()
{
	while (1)
	{
		int f;
		printf("1.录入新书\n");
		printf("2.查询书籍信息\n");
		printf("3.显示全部图书信息\n");
		printf("4.删除图书\n");
		printf("请输入你要选择的操作前的数字：");
		cin >> f;
		if (f >= 1 && f <= 4)
		{
			switch (f)
			{
			case 1:
			{
				addbook();
				break;
			}
			case 2:
			{
				string name;
				cout << "请输入书名：";		cin >> name;
				book* t = findbook(name);
				if (t == NULL)
					printf("输入的图书不存在\n");
				else
					(*t).display();
				break;
			}
			case 3:
			{
				for (int i = 0; i < bookList.size(); i++)
					bookList[i].display();
				break;
			}
			case 4:
			{
				string name;
				cout << "请输入书名：";		cin >> name;
				if (delbook(name))
					cout << "删除成功！" << endl;
				else
					cout << "没有这本图书！" << endl;
				break;
			}
			}
		}
		else
		{
			printf("输入错误！\n");
		}
	}
}