#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class teacher
{
public:
	int no;
	string name;
	string g;
	string work;
	string phone;
	int baseWage;
	int allowance;
	int subsidy;
	int telRate;
	int waterRate;
	int rent;
	int tax;
	int senRate;
	int fund;

	int getWage()
	{
		return baseWage + allowance + subsidy;
	}

	int getConsume()
	{
		return telRate + waterRate + rent + tax + senRate + fund;
	}

	int getRealWage()
	{
		return getWage() - getConsume();
	}

	void display()
	{
		cout << "----" << this->name << " " << this->no << "----\n";
		cout << "性别：" << this->g << endl;
		cout << "工作单位：" << this->work << endl;
		cout << "手机号码：" << this->phone << endl;
		cout << "基础工资：" << this->baseWage << endl;
		cout << "津贴：" << this->allowance << endl;
		cout << "生活补贴：" << this->subsidy << endl;
		cout << "电话费：" << this->phone << endl;
		cout << "水电费：" << this->waterRate << endl;
		cout << "房租:" << this->rent << endl;
		cout << "所得税：" << this->tax << endl;
		cout << "卫生费:" << this->senRate << endl;
		cout << "公积金:" << this->fund << endl;
		cout << "应发工资：" << this->getWage() << endl;
		cout << "合计扣款：" << this->getConsume() << endl;
		cout << "实际工资：" << this->getRealWage() << endl;
		cout << "--------" << endl;
	}
};

class teacherManage
{
public:
	static vector<teacher> teacherList;
	static void addTeacher()
	{
		teacherManage::teacherList.push_back(teacher());
		teacher &t = teacherList.back();
		cout << "请输入要录入的教师信息：" << endl;
		cout << "工号：";	cin >> t.no;
		cout << "姓名：";	cin >> t.name;
		cout << "性别（男/女）：";		cin >> t.g;
		cout << "工作单位：";		cin >> t.work;
		cout << "手机号码：";		cin >> t.phone;
		cout << "基础工资：";		cin >> t.baseWage;
		cout << "津贴：";	cin >> t.allowance;
		cout << "生活补贴：";		cin >> t.subsidy;
		cout << "电话费：";	cin >> t.telRate;
		cout << "水电费：";	cin >> t.waterRate;
		cout << "房租：";	cin >> t.rent;
		cout << "所得税：";	cin >> t.tax;
		cout << "卫生费：";	cin >> t.senRate;
		cout << "公积金：";	cin >> t.fund;
		cout << "录入完成" << endl;
	}

	static teacher* findTeacher(string name)
	{
		for (int t=0;t<teacherList.size();t++)
		{
			if (teacherList[t].name == name)
				return &teacherList[t];
		}
		return NULL;
	}

	static teacher* findTeacher(int no)
	{
		for (int t=0;t<teacherList.size();t++)
		{
			if (teacherList[t].no == no)
				return &teacherList[t];
		}
		return NULL;
	}

	static void modifyTeacher(teacher &t)
	{
		cout << "请输入新的财务信息" << endl;
		cout << "基础工资：";		cin >> t.baseWage;
		cout << "津贴：";	cin >> t.allowance;
		cout << "生活补贴：";		cin >> t.subsidy;
		cout << "电话费：";	cin >> t.telRate;
		cout << "水电费：";	cin >> t.waterRate;
		cout << "房租：";	cin >> t.rent;
		cout << "所得税：";	cin >> t.tax;
		cout << "卫生费：";	cin >> t.senRate;
		cout << "公积金：";	cin >> t.fund;
		cout << "修改完成" << endl;
	}
};
vector<teacher> teacherManage::teacherList;

teacher* inputName()
{
	string name;
	cout << "请输入教师姓名：";		cin >> name;
	teacher* t = teacherManage::findTeacher(name);
	return t;
}

int main()
{
	while (1)
	{
		int f;
		printf("1.录入新的教师信息\n");
		printf("2.修改教师信息\n");
		printf("3.查询教师信息\n");
		printf("4.显示全部教师信息\n");
		printf("请输入你要选择的操作前的数字：");
		scanf("%d", &f);
		if (f >= 1 && f <= 4)
		{

			switch (f)
			{
			case 1:
			{
				teacherManage::addTeacher();
				break;
			}
			case 2:
			{
				teacher* t = inputName();
				if (t == NULL)
					printf("输入的教师不存在\n");
				else
					teacherManage::modifyTeacher(*t);
				break;
			}
			case 3:
			{
				teacher* t = inputName();
				if (t == NULL)
					printf("输入的教师不存在\n");
				else
					(*t).display();
				break;
			}
			case 4:
			{
				for (int i=0;i<teacherManage::teacherList.size();i++)
					teacherManage::teacherList[i].display();
			}
			}
		}
		else
		{
			printf("输入错误！\n");
		}
	}
}
