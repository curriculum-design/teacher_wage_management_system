#include "pch.h"
#include <stdio.h>
#include <cstring>
#include <Windows.h>

bool isEq(char c1[], char c2[])
{
	return strcmp(c1, c2) == 0;
}

struct user
{
	char username[100];
	char password[100];
};

user userList[100];
int userNum = 0;

user* findUser(char username[])
{
	for (int i = 0; i < userNum; i++)
	{
		if (isEq(userList[i].username, username))
			return &userList[i];
	}
	return nullptr;
}

bool passwordRight(user* u, char password[])
{
	return isEq(u->password, password);
}

int main()
{
	while (1)
	{
		printf("1.注册\n");
		printf("2.登陆\n");
		int choose;
		scanf_s("%d", &choose);
		if (choose == 1)
		{
			user u;
			printf("请输入用户名：");
			scanf_s("%s", u.username,100);
			printf("请输入密码：");
			scanf_s("%s", u.password,100);
			userList[userNum] = u;
			userNum += 1;
			printf("注册成功！\n");
		}
		else if (choose == 2)
		{
			char c[100];
			printf("请输入用户名：");
			scanf_s("%s", c, 100);
			user* u = findUser(c);
			if (u == nullptr)
			{
				printf("找不到这个用户！\n");
			}
			else
			{
				printf("请输入密码：");
				scanf_s("%s", c, 100);
				if (passwordRight(u, c))
				{
					printf("登陆成功！\n");
				}
				else
				{
					printf("密码错误！\n");
				}
			}
			getchar();
		}
		getchar();
		system("cls");
	}
}

